class ApplicationController < ActionController::Base
  helper_method :current_user, :logged_in?
  before_action :require_authentication

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  def current_user
    user = User.find(session[:user_id]) if session[:user_id]
    @current_user ||= user
  end

  def logged_in?
    current_user != nil
  end

  def require_authentication
    redirect_to login_path unless logged_in?
  end
end
