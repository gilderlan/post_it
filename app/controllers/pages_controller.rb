class PagesController < ApplicationController
  skip_before_action :require_authentication
  
  def home
    @q = Post.ransack(params[:q])
    @posts = @q.result.paginate(page: params[:page])
  end
end
