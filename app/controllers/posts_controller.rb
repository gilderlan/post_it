class PostsController < ApplicationController
  before_action :load_post, only: [:destroy, :show]

  def index
    @q = Post.ransack(params[:q])
    @posts = @q.result(distinct: true).page(params[:page])
  end

  def new
    @post = Post.new
  end

  def show
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "Post cadastrado com sucesso"
      redirect_to post_path(@post)
    else
      flash.now[:warning] = "Erro ao cadastrar post"
      render :new
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "Post excluído com sucesso"
    redirect_to root_path
  end

  private
  def post_params
    params.require(:post).permit(:title, :body)
  end

  def load_post
    @post = Post.find(params[:id])
  end
end
