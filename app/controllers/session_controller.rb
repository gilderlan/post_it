class SessionController < ApplicationController
  skip_before_action :require_authentication

  def new
  end

  def create
    user = User.find_by(username: params[:username])

    if user && user.authenticate(params[:password])
      reset_session
      session[:user_id] = user.id
      redirect_to root_path
    else
      flash.now[:error] = "Credenciais inválidas"
      render :new
    end
  end

  def destroy
    reset_session
    redirect_to login_path
  end
end
