class UsersController < ApplicationController
  skip_before_action :require_authentication, only: [:show]
  
  def show
    @user = User.find(params[:id])
  end
end
