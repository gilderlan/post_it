class VotesController < ApplicationController
  def create
    @vote = current_user.votes.build(vote_params)

    if @vote.save
      flash[:success] = "Voto cadastrado com sucesso"
    else
      flash[:error] = "Erro ao cadastrar voto"
    end

    redirect_back(fallback_location: root_path)
  end

  def vote_params
    params.require(:vote).permit(:kind, :post_id)
  end
end
