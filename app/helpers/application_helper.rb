module ApplicationHelper
  def post_link(name, sort_by, order = "desc")
    sort = {q: {s: ["#{sort_by} #{order}"]}}
    css_class = params["q"].try(:[], "s").try(:first).try(:include?, sort_by) ? "active item" : "item"
    link_to name, root_path(params: sort), {class: css_class}
  end
end
