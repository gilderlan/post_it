class Post < ApplicationRecord
  belongs_to :user

  validates :title, presence: true
  validates :body, presence: true

  has_many :votes, dependent: :destroy

  # def vote_balance
  #   vote_balance || 0
  # end
end
