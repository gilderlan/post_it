class Vote < ApplicationRecord
  after_create :update_vote_balance
  after_destroy :revert_vote

  belongs_to :user
  belongs_to :post

  validates :kind, presence: true
  validates :kind, uniqueness: {scope: [:post_id, :user_id]}

  def update_vote_balance
    balance = self.post.vote_balance || 0

    balance += 1 if self.kind == "up"
    balance -= 1 if self.kind == "down"

    self.post.update(vote_balance: balance);
  end

  def revert_vote
    balance = self.post.vote_balance || 0
    value = self.kind == "up" ? -1 : +1

    self.post.update(vote_balance: balance + value);
  end
end
