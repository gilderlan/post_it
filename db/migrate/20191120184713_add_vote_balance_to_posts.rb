class AddVoteBalanceToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :vote_balance, :integer, default: 0
  end
end
