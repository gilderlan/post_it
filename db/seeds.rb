30.times do
  user = User.new(
    username: Faker::Internet.username,
    bio: Faker::Lorem.sentence
  )

  puts "USER: #{user.username} - created" if user.save
end

100.times do
  post = Post.new(
    user: User.all.sample,
    title: Faker::Lorem.sentence,
    body: Faker::Lorem.paragraph
  )

  puts "POST: #{post.title} - created" if post.save
end

500.times do
  vote = Vote.new(
    user: User.all.sample,
    post: Post.all.sample,
    kind: ["up", "down"].sample
  )

  puts "VOTE: #{vote.id} - created" if vote.save
end
