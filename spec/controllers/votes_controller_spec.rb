require 'rails_helper'

RSpec.describe VotesController, type: :controller do
  context "Create vote" do
    it "success" do
      post = create(:post)

      vote_params = {
        post_id: post.id,
        kind: "up"
      }

      post(:create, params: {vote: vote_params})
      expect(response.status).to eql(200)
    end
  end
end
