FactoryBot.define do
  sequence :username do |i|
    "#{Faker::Internet.username}-#{i}"
  end

  factory :user do
    username
    bio {Faker::Lorem.sentence}
  end
end
