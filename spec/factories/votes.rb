FactoryBot.define do
  factory :vote do
    user
    post
    kind {["up", "down"].sample}
  end
end
