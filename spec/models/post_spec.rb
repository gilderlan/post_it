require 'rails_helper'

RSpec.describe Post, type: :model do
  context "Validations" do
    before do
      @post = build(:post)
    end

    it "post deve ser valido" do
      expect(@post).to be_valid
    end

    it "user deve ser obrigatorio" do
      @post.user = nil
      expect(@post).to_not be_valid
    end

    it "title deve ser obrigatorio" do
      @post.title = nil
      expect(@post).to_not be_valid
    end

    it "body deve ser obrigatorio" do
      @post.body = nil
      expect(@post).to_not be_valid
    end
  end
end
