require 'rails_helper'

RSpec.describe User, type: :model do
  context "Validations" do
    before do
      @user = build(:user)
    end

    it "should be valid" do
      expect(@user).to be_valid
    end

    it "o campo username deve ser obrigatorio" do
      @user.username = nil
      expect(@user).to_not be_valid
    end

    it "o campo username deve ser unico" do
      username = Faker::Internet.username
      User.create(username: username)

      user = User.new(username: username)
      expect(user).to_not be_valid
    end
  end
end
