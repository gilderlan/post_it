require 'rails_helper'

RSpec.describe Vote, type: :model do
  context "Validations" do
    before do
      @vote = build(:vote)
    end

    it "vote deve ser valido" do
      expect(@vote).to be_valid
    end

    it "user deve ser obrigatorio" do
      @vote.user = nil
      expect(@vote).to_not be_valid
    end

    it "post deve ser obrigatorio" do
      @vote.post = nil
      expect(@vote).to_not be_valid
    end

    it "kind deve ser obrigatorio" do
      @vote.kind = nil
      expect(@vote).to_not be_valid
    end

    it "se nao houver votes o vote_balance deve ser nil" do
      post = Post.create(
        user: @user,
        title: Faker::Lorem.sentence,
        body: Faker::Lorem.paragraph
      )

      expect(post.vote_balance).to eql(nil)
    end


    it "deve adicionar um vote no vote_balance" do
      post = create(:post)
      vote = create(:vote, post: post, kind: "up")

      post.reload
      expect(post.vote_balance).to eql(1)
    end

    it "deve subtrair um vote no vote_balance" do
      post = create(:post)
      vote = create(:vote, post: post, kind: "down")

      post.reload
      expect(post.vote_balance).to eql(-1)
    end

    it "deve subtrair do vote_balance se voto up for removido" do
      post = create(:post)
      vote = create(:vote, post: post, kind: "up")

      vote.destroy
      post.reload

      expect(post.vote_balance).to eql(0)
    end

    it "deve adicionar 1 ao vote_balance se voto down for removido" do
      post = create(:post)
      vote = create(:vote, post: post, kind: "down")

      vote.destroy
      post.reload

      expect(post.vote_balance).to eql(0)
    end
  end
end
